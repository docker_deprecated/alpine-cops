# alpine-cops

#### [alpine-x64-cops](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-cops/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64build/alpine-x64-cops.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-cops "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64build/alpine-x64-cops.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-cops "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-cops.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-cops/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-cops.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-cops/)
#### [alpine-aarch64-cops](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-cops/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64build/alpine-aarch64-cops.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-cops "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64build/alpine-aarch64-cops.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-cops "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-cops.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-cops/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-cops.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-cops/)
#### [alpine-armhf-cops](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-cops/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhfbuild/alpine-armhf-cops.svg)](https://microbadger.com/images/forumi0721alpinearmhfbuild/alpine-armhf-cops "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhfbuild/alpine-armhf-cops.svg)](https://microbadger.com/images/forumi0721alpinearmhfbuild/alpine-armhf-cops "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-cops.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-cops/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-cops.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-cops/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [cops](http://blog.slucas.fr/en/oss/calibre-opds-php-server)
    - Calibre OPDS (and HTML) PHP Server : web-based light alternative to Calibre content server / Calibre2OPDS to serve ebooks (epub, mobi, pdf, ...)
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 8000:8000/tcp \
           forumi0721alpinex64build/alpine-x64-cops:latest
```

* aarch64
```sh
docker run -d \
           -p 8000:8000/tcp \
           forumi0721alpineaarch64build/alpine-aarch64-cops:latest
```

* armhf
```sh
docker run -d \
           -p 8000:8000/tcp \
           forumi0721alpinearmhfbuild/alpine-armhf-cops:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:8000/](http://localhost:8000/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8000/tcp           | Serivce port                                     |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| COPS_TITLE         | Service title                                    |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-cops](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-cops/)
* [forumi0721alpinearmhfbuild/alpine-armhf-cops](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-cops/)
* [forumi0721alpineaarch64build/alpine-aarch64-cops](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-cops/)

